from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
from ForBestPriceApi import settings

class Conexao():

	__instacia_singleton = None # saca a malandragem

	def __init__(self):
		if Conexao.__instacia_singleton == None:
			self.ap = PlainTextAuthProvider(username=settings.DATABASES['default']['USER'], password=settings.DATABASES['default']['PASSWORD'])
			self.cluster = Cluster([settings.DATABASES['default']['HOST']], protocol_version=2, auth_provider=self.ap) #
			self.con = self.cluster.connect(settings.DATABASES['default']['NAME'])
			Conexao.__instacia_singleton = self


	def __new__(cls):
		if cls.__instacia_singleton != None:
			return cls.__instacia_singleton
		else:
			return super(Conexao, cls).__new__(cls)
