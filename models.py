from .service import ModelCassandra, campo

class Usuario(ModelCassandra):

    __tabela__ = 'usuario'
    def __init__(self):
        self.email = campo.Texto(primary_key=True, coluna_nome='email')
        self.produtosmonitorar = campo.Texto(coluna_nome='produtosmonitorar')
        self.senha = campo.Texto(coluna_nome='senha')
        ModelCassandra.__init__(self)
