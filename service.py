from ForBestPriceApi.DataBase import Conexao

session = Conexao().con

class ModelCassandra():

    def __init__(self, *args, **kwargs):
        self.objects = ObjectsCassandra(self)

class ObjectsCassandra(object):

    def __init__(self, model):
        self.model = model

    def row_to_model(self, rows):
        if not rows: return None
        __list_objs = []
        for row in rows:
            ob = self.model.__class__()
            attrs = [x for x in ob.__dir__() if not x.startswith('__')]
            for x, y in row.__dict__.items():
                if hasattr(self.model.__class__, x):
                    if hasattr(getattr(ob, x), 'coluna_nome'):
                        if getattr(ob, x).coluna_nome == x:
                            setattr(getattr(ob, x), 'valor', y)
                    else:
                        setattr(getattr(ob, x), 'valor', y)
                else:
                    for k in attrs:
                        if hasattr(getattr(ob, k), 'coluna_nome') and getattr(ob, k).coluna_nome == x:
                            setattr(getattr(ob, k), 'valor', y)
                            break
            __list_objs.append(ob)
        return __list_objs

    def all(self, **kwargs):
        __query = "select * from %s" % self.model.__tabela__
        __has_where = False
        if '__start' in kwargs:
            ob = self.model.__class__()
            attrs = [x for x in ob.__dir__() if not x.startswith('__')]
            for x in attrs:
                if hasattr(getattr(ob, x), 'primary_key') and getattr(ob, x).primary_key:
                    __has_where = True
                    if hasattr(getattr(ob, x), 'coluna_nome'):
                        __query+= " WHERE token(%s) > token('%s')" % (getattr(ob, x).coluna_nome, kwargs['__start'])
                    else:
                        __query+= " WHERE token(%s) > token('%s')" % (x, kwargs['__start'])
        if '__limit' in kwargs:
            __query += " limit %s" % kwargs['__limit']

        print(__query)
        rows = session.execute(__query)
        return self.row_to_model(rows)

    def filter(self, **kwargs):
        __query = "select * from %s" % self.model.__tabela__
        __filters = ""
        __filters += [" %s = %s " % (k, v) for k, v in kwargs.items()]
        return session.execute(__query+__filters)



class Field():

    def __init__(self, *args, **kwargs):
        self.valor = None
        for k, v in kwargs.items():
            if k == 'primary_key':
                self.primary_key = bool(v)
            elif k == 'coluna_nome':
                self.coluna_nome = str(v)

    def __repr__(self):
        return str(self.valor)


class campo():

    class Texto(Field):

        def __init__(self, *args, **kwargs):
            Field.__init__(self, *args, **kwargs)
